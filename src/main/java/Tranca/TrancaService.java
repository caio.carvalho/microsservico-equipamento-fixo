package Tranca;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class TrancaService {
	
	public TrancaService(){		
	}

	//Base
	
    public static Map<Integer, Tranca> trancas = new HashMap<>();
    public static AtomicInteger lastId;
     
    static {
        trancas.put(1, new Tranca(1, 123, "Em Reparo"));
        trancas.put(2, new Tranca(2, 654, "Disponivel"));
        trancas.put(3, new Tranca(3, 365, "Em Reparo"));
        trancas.put(4, new Tranca(4, 422, "Disponivel"));
        trancas.put(5, new Tranca(5, 127, "Disponivel"));
        trancas.put(6, new Tranca(6, 684, "Em Reparo"));
        trancas.put(7, new Tranca(7, 666, "Disponivel"));
        trancas.put(8, new Tranca(8, 444, "Em Reparo"));
        trancas.put(9, new Tranca(9, 9874, "Ocupada"));
        trancas.put(10, new Tranca(10, 985, "Disponivel"));
        trancas.put(11, new Tranca(11, 426, "Disponivel"));
        trancas.put(12, new Tranca(12, 657, "Disponivel"));
        trancas.put(13, new Tranca(13, 951, "Aposentada"));
        trancas.put(14, new Tranca(14, 657, "Ocupada"));
        trancas.put(15, new Tranca(15, 251, "Em Reparo"));
        lastId = new AtomicInteger(trancas.size());
    }

    //Operacoes de Base
    
    public static Collection<Tranca> GetAllTrancas() {
        return trancas.values();
    }
    
    public static Tranca FindTrancaById(int idTranca) {
        return trancas.get(idTranca);
    }

    public static void AddTranca(int numero) {
        int id = lastId.incrementAndGet();
        trancas.put(id, new Tranca(id, numero, "Disponivel"));
    }
 
    public static void UpdateStatusTranca(int idTranca, int numero, String status) {
        trancas.put(idTranca, new Tranca(idTranca, numero, status));
    }    

    public static void DeleteTranca(int idTranca) {
        trancas.remove(idTranca);
    }
    
    // Operacoes de memoria
    
    public static Tranca FindTrancaByNumero(int numero) {
    	Tranca retornoTranca = null;
    	
    	for(Tranca tranca : trancas.values()) {
    		if(tranca.numero == numero) {
    			retornoTranca = tranca;
    		}
    	}
    	return retornoTranca;
    }
    
    public static String getStatusTrancaById(int idTranca) {
    	Tranca trancaEncontrada = FindTrancaById(idTranca);    	
    	if(trancaEncontrada != null) {
    		return trancaEncontrada.status;
    	}
		return null;
    }
    
    public static String GetStatusTrancaByNumero(int numero) {
    	Tranca trancaEncontrada = FindTrancaByNumero(numero);
    	if(trancaEncontrada != null) {    		
    		return trancaEncontrada.status;    			
    	}   	
		return null;    	
    }
}