package Tranca;

public class Tranca {
	public int id;
	public int numero;
	public String status;
	
	public Tranca(int id, int numero, String status) {
		this.id = id;
		this.numero = numero;
		this.status = status;
	}
}