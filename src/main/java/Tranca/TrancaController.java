package Tranca;

import Equipamento.ErrorResponse;
import io.javalin.http.BadRequestResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.javalin.plugin.openapi.annotations.*;

public class TrancaController {
	
	public TrancaController() {		
	}

	private static int getIdTrancaByCtx(Context ctx) {
		int idTranca;
		String idTrancaString = ctx.queryParam("trancaId");
		if(idTrancaString != null) {
			idTranca = Integer.parseInt(idTrancaString);
		}
		else {
			idTranca = 0;
		}
		return idTranca;
	}
	
	private static int getNumeroTrancaByCtx(Context ctx) {
		int numero;
		String numeroString = ctx.queryParam("numero");
		if(numeroString != null) {
			numero = Integer.parseInt(numeroString);
		}
		else {
			numero = 0;
		}
		return numero;
	}
	
	private static boolean statusTrancaIsValid(Context ctx) {
		if(	(ctx.queryParam("statusTranca") == null) ||
			!ctx.queryParam("statusTranca").equals("Disponivel") && 
			!ctx.queryParam("statusTranca").equals("Ocupada") && 
			!ctx.queryParam("statusTranca").equals("Em Reparo") && 
			!ctx.queryParam("statusTranca").equals("Aposentada") )
		{
				return false;
		}
		return true;
	}
	
	@OpenApi(
		    path = "/tranca",            // only necessary to include when using static method references
		    method = HttpMethod.GET,    // only necessary to include when using static method references
		    summary = "Retorna todas as trancas cadastradas",
		    operationId = "getTrancas",
		    tags = {"Tranca"},
		    responses = {
		        @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Tranca[].class)})
		    }
	)	
    public static void getAllTrancas(Context ctx) {
		ctx.json(TrancaService.GetAllTrancas());
		ctx.status(200);
    }
	
	@OpenApi(

            path = "/tranca/buscaPorId",
            summary = "Busca tranca pelo ID ou pelo numero",
            operationId = "getTrancaById",
            method = HttpMethod.GET,
            tags = {"Tranca"},
            queryParams = {
            		@OpenApiParam(name = "trancaId", type = Integer.class, description = "ID da Tranca"),
               		@OpenApiParam(name = "numero", type = Integer.class, description = "Numero da Tranca")
               },
            responses = {
                    @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Tranca.class)}),
                    @OpenApiResponse(status = "400", content = {@OpenApiContent(from = ErrorResponse.class)}),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )
    public static void getTrancaByIdNumero(Context ctx) {
		Tranca trancaResposta = null;
		int idTranca = getIdTrancaByCtx(ctx);
		int numeroTranca = getNumeroTrancaByCtx(ctx);
		
		if(numeroTranca == 0) {
			trancaResposta = TrancaService.FindTrancaById(idTranca);
		} 
		if (idTranca == 0) {
			trancaResposta = TrancaService.FindTrancaByNumero(numeroTranca);
		}
        if (trancaResposta == null) {
            throw new NotFoundResponse("Tranca nao encontrada");
        } else {
            ctx.json(trancaResposta);
            ctx.status(200);
        }
    }
	
	@OpenApi(
            path = "/tranca/deletaPorId",
			summary = "Deleta tranca pelo ID",
            operationId = "deleteTrancaById",
            method = HttpMethod.DELETE,
            tags = {"Tranca"},
            queryParams = {
            		@OpenApiParam(name = "trancaId", type = Integer.class, description = "ID da Tranca")
            },
            responses = {
                    @OpenApiResponse(status = "204"),
                    @OpenApiResponse(status = "400", content = {@OpenApiContent(from = ErrorResponse.class)}),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )
    public static void deleteTranca(Context ctx) {
        Tranca tranca = TrancaService.FindTrancaById(getIdTrancaByCtx(ctx));
        if (tranca == null) {
            throw new NotFoundResponse("Tranca nao encontrada");
        } else {
        	TrancaService.DeleteTranca(tranca.id);
            ctx.status(204);
        }
    }
	
	@OpenApi(
		    path = "/tranca",           
		    method = HttpMethod.POST,    
		    summary = "Adiciona um novo cadastro de tranca",
		    operationId = "cadastrarTranca",
		    tags = {"Tranca"},
		    requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = NewTrancaRequest.class)}),		
		    responses = {
		        @OpenApiResponse(status = "201", content = {@OpenApiContent(from = Tranca[].class)}),
		        @OpenApiResponse(status = "400", content = {@OpenApiContent(from = ErrorResponse.class)})
		    }
	)
    public static void addTranca(Context ctx) {
		NewTrancaRequest tranca = ctx.bodyAsClass(NewTrancaRequest.class);
        TrancaService.AddTranca(tranca.numero);
        ctx.status(201);
    }

	 @OpenApi(
	            summary = "Altera o status de uma tranca",
	            operationId = "updateTrancaById",
	            path = "/statusTranca",
	            method = HttpMethod.PATCH,
	            queryParams = {
	            		@OpenApiParam(name = "trancaId", type = Integer.class, description = "ID da Tranca"),
	            		@OpenApiParam(name = "statusTranca", type = String.class, description = "Status da Tranca")
	            },
	            tags = {"Tranca"},
	            responses = {
	                    @OpenApiResponse(status = "204"),
	                    @OpenApiResponse(status = "400", content = {@OpenApiContent(from = ErrorResponse.class)}),
	                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)})
	            }
	    )
	    public static void updateStatusTranca(Context ctx) {
	        Tranca tranca = TrancaService.FindTrancaById(getIdTrancaByCtx(ctx));
	        if (tranca == null) {
	            throw new NotFoundResponse("Tranca nao encontrada");
	        } else if(!statusTrancaIsValid(ctx)){
	        	{
	        		throw new BadRequestResponse("Status invalido");
	        	}
	        } else {
	            TrancaService.UpdateStatusTranca(tranca.id, tranca.numero, ctx.queryParam("statusTranca"));
	            ctx.status(204);
	        }
	    }
	 
	 @OpenApi(

	            path = "/statusTranca",
	            summary = "Busca status da Tranca pelo ID ou pelo ID e pelo numero",
	            operationId = "getStatusTrancaByIdNumero",
	            method = HttpMethod.GET,
	            tags = {"Tranca"},
	            queryParams = {
	            		@OpenApiParam(name = "trancaId", type = Integer.class, description = "ID da Tranca"),
	               		@OpenApiParam(name = "numero", type = Integer.class, description = "Numero da Tranca")
	               },
	            responses = {
	                    @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Tranca.class)}),
	                    @OpenApiResponse(status = "400", content = {@OpenApiContent(from = ErrorResponse.class)}),
	                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)})
	            }
	    )
	    public static void getStatusTrancaByIdNumero(Context ctx) {
		 String trancaResposta = null;
			int idTranca = getIdTrancaByCtx(ctx);
			int numeroTranca = getNumeroTrancaByCtx(ctx);
			
			if(numeroTranca == 0) {
				trancaResposta = TrancaService.getStatusTrancaById(idTranca);
			} 
			if (idTranca == 0) {
				trancaResposta = TrancaService.GetStatusTrancaByNumero(numeroTranca);
			}
	        if (trancaResposta == null) {
	            throw new NotFoundResponse("Tranca nao encontrada");
	        } else {
	            ctx.json(trancaResposta);
	            ctx.status(200);
	        }
	    }
	   
	 @OpenApi(

	            path = "/liberaTranca",
	            summary = "Busca status da Tranca pelo ID ou pelo ID e pelo numero",
	            operationId = "liberaTrancaById",
	            method = HttpMethod.PATCH,
	            tags = {"Tranca"},
	            queryParams = {
	            		@OpenApiParam(name = "trancaId", type = Integer.class, description = "ID da Tranca")
	               },
	            responses = {
	                    @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Tranca.class)}),
	                    @OpenApiResponse(status = "400", content = {@OpenApiContent(from = ErrorResponse.class)}),
	                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)})
	            }
	    )
	    public static void liberaTrancaById(Context ctx) {
		 Tranca tranca = TrancaService.FindTrancaById(getIdTrancaByCtx(ctx));
	        if (tranca == null) {
	        	throw new NotFoundResponse("Tranca nao encontrada");
	        } else if(tranca.status.equals("Ocupada")){
	        	{
	        		TrancaService.UpdateStatusTranca(tranca.id, tranca.numero, "Disponivel");
	 	            ctx.status(204);
	        	}
	        } else {
	        	if(tranca.status.equals("Em Reparo"))
	        		throw new BadRequestResponse("Essa tranca se encontra em reparo");
	        	if(tranca.status.equals("Aposentada"))
	        		throw new BadRequestResponse("Essa tranca se encontra aposentada");
	        	if(tranca.status.equals("Disponivel"))
	        		throw new BadRequestResponse("Essa tranca ja se enontra liberada!");
	        }
	    }

}