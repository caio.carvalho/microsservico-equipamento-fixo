package Equipamento;

import java.util.Map;

public class ErrorResponse {
	
	public ErrorResponse() {		
	}
	
    public String title;
    public int status;
    public String type;
    public Map<String, String> details;
}