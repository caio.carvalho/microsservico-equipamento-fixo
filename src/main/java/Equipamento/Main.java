package Equipamento;

public class Main {

    public static void main(String[] args) {
        int port = 7000; 
    	JavalinApp app = new JavalinApp();
    	app.start(getHerokuAssignedPort(port));
        System.out.println("Check out ReDoc docs at http://localhost:7000/redoc");
        System.out.println("Check out Swagger UI docs at http://localhost:7000/swagger-ui");
    }
    
    static int getHerokuAssignedPort(int port) {
        String herokuPort = System.getenv("PORT");
        if (herokuPort != null) {
          return Integer.parseInt(herokuPort);
        }
        return port;
      }
}