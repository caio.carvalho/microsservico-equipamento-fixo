package Equipamento;

import io.javalin.Javalin;
import io.javalin.plugin.openapi.OpenApiOptions;
import io.javalin.plugin.openapi.OpenApiPlugin;
import io.javalin.plugin.openapi.ui.ReDocOptions;
import io.javalin.plugin.openapi.ui.SwaggerOptions;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.models.info.Info;

import static io.javalin.apibuilder.ApiBuilder.*;

import Totem.TotemController;
import Tranca.TrancaController;

public class JavalinApp {

   private Javalin app = 
    		Javalin.create(config -> {
                config.registerPlugin(getConfiguredOpenApiPlugin());
                config.defaultContentType = "application/json";
            }).routes(() -> {
                path("tranca", () -> {
                    get(TrancaController::getAllTrancas);
                    post(TrancaController::addTranca);
                    path("/buscaPorId", () -> {
                    	get(TrancaController::getTrancaByIdNumero);
                    });
                    path("/deletaPorId", () -> {                    
                        delete(TrancaController::deleteTranca);
                    });
                });          
                path("statusTranca", () -> {                
                    patch(TrancaController::updateStatusTranca);
                    get(TrancaController::getStatusTrancaByIdNumero);               
                });
                path("liberaTranca", () -> {                
                    patch(TrancaController::liberaTrancaById);              
                });
                path("totem", () -> {
                    get(TotemController::getAllTotens);
                    post(TotemController::addTotem);
                    path("/buscaPorId", () -> {
                    	get(TotemController::getTotemByIdNumero);                		
                    });
                    path("/deletaPorId", () -> {
                    	delete(TotemController::deleteTotem);                		
                    });           
                    
                });  
            });
   
   public void start (int port) {
	   this.app.start(port);
   }
   
   public void stop() {
	   this.app.stop();
   }

    static OpenApiPlugin getConfiguredOpenApiPlugin() {
        Info info = new Info().version("1.0").description("User API");
        OpenApiOptions options = new OpenApiOptions(info)
                .activateAnnotationScanningFor("Tranca", "Totem")
                .path("/swagger-docs") // endpoint for OpenAPI json
                .swagger(new SwaggerOptions("/swagger-ui")) // endpoint for swagger-ui
                .reDoc(new ReDocOptions("/redoc")) // endpoint for redoc
                .defaultDocumentation(doc -> {
                    doc.json("500", ApiResponse.class);
                    doc.json("503", ApiResponse.class);
                });
        return new OpenApiPlugin(options);
    }
   
}