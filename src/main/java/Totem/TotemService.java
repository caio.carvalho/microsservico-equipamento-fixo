package Totem;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

public class TotemService {
	
	public TotemService() {		
	}

	//Base
	
    public static Map<Integer, Totem> totens = new HashMap<>();
    public static AtomicInteger lastId;
     
    static {
        totens.put(1, new Totem(1, 235));
        totens.put(2, new Totem(2, 344));
        totens.put(3, new Totem(3, 522));
        totens.put(4, new Totem(4, 312));
        totens.put(5, new Totem(5, 420));
        totens.put(6, new Totem(6, 597));
        totens.put(7, new Totem(7, 894));
        totens.put(8, new Totem(8, 347));
        totens.put(9, new Totem(9, 652));
        lastId = new AtomicInteger(totens.size());
    }

    //Operacoes de Base
    
    public static Collection<Totem> GetAllTotens() {
        return totens.values();
    }
    
    public static Totem FindTotemById(int idTotem) {
        return totens.get(idTotem);
    }

    public static void AddTotem(int numero) {
        int id = lastId.incrementAndGet();
        totens.put(id, new Totem(id, numero));
    }
    
    public static void DeleteTotem(int idTotem) {
        totens.remove(idTotem);
    }
    
    // Operacoes de memoria
    
    public static Totem FindTotemByNumero(int numero) {
    	Totem retornoTotem = null;
    	
    	for(Totem totem : totens.values()) {
    		if(totem.numero == numero) {
    			retornoTotem = totem;
    		}
    	}
    	return retornoTotem;
    }
    
    
    
    
    
}