package Totem;

import Equipamento.ErrorResponse;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.javalin.plugin.openapi.annotations.*;

public class TotemController {
	
	public TotemController() {		
	}
	
	private static int getIdTotemByCtx(Context ctx) {
		int idTotem;
		String idTotemString = ctx.queryParam("totemId");
		if(idTotemString != null) {
			idTotem = Integer.parseInt(idTotemString);
		}
		else {
			idTotem = 0;
		}
		return idTotem;
	}
	
	private static int getNumeroTotemByCtx(Context ctx) {
		int numero;
		String numeroString = ctx.queryParam("numero");
		if(numeroString != null) {
			numero = Integer.parseInt(numeroString);
		}
		else {
			numero = 0;
		}
		return numero;
	}

	@OpenApi(
		    path = "/totem",            // only necessary to include when using static method references
		    method = HttpMethod.GET,    // only necessary to include when using static method references
		    summary = "Retorna todos os totens cadastrados",
		    operationId = "getTotens",
		    tags = {"Totem"},
		    responses = {
		        @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Totem[].class)})
		    }
	)	
    public static void getAllTotens(Context ctx) {
		ctx.json(TotemService.GetAllTotens());
		ctx.status(200);
    }
	
	@OpenApi(

            path = "/totem/buscaPorId",
            summary = "Busca totem pelo ID ou pelo numero",
            operationId = "getTtotemById",
            method = HttpMethod.GET,
            tags = {"Totem"},            
            queryParams = {
            		@OpenApiParam(name = "totemId", type = Integer.class, description = "ID do Totem"),
               		@OpenApiParam(name = "numero", type = Integer.class, description = "Numero do Totem")
               },
            responses = {
                    @OpenApiResponse(status = "200", content = {@OpenApiContent(from = Totem.class)}),
                    @OpenApiResponse(status = "400", content = {@OpenApiContent(from = ErrorResponse.class)}),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )
    public static void getTotemByIdNumero(Context ctx) {
		Totem totemResposta = null;
		int idTotem = getIdTotemByCtx(ctx);
		int numeroTotem = getNumeroTotemByCtx(ctx);
		
		if(numeroTotem == 0) {
			totemResposta = TotemService.FindTotemById(idTotem);
		} 
		if (idTotem == 0) {
			totemResposta = TotemService.FindTotemByNumero(numeroTotem);
		}
        if (totemResposta == null) {
            throw new NotFoundResponse("Totem nao encontrado");
        } else {
            ctx.json(totemResposta);
            ctx.status(200);
        }
    }
	
	@OpenApi(
            path = "/totem/deletaPorId",
			summary = "Deleta totem pelo ID",
            operationId = "deleteTotemById",
            method = HttpMethod.DELETE,
            queryParams = {
                    @OpenApiParam(name = "totemId", type = Integer.class, description = "ID do Totem")
                       },
            tags = {"Totem"},
            responses = {
                    @OpenApiResponse(status = "204"),
                    @OpenApiResponse(status = "400", content = {@OpenApiContent(from = ErrorResponse.class)}),
                    @OpenApiResponse(status = "404", content = {@OpenApiContent(from = ErrorResponse.class)})
            }
    )
    public static void deleteTotem(Context ctx) {
        Totem totem = TotemService.FindTotemById(getIdTotemByCtx(ctx));
        if (totem == null) {
            throw new NotFoundResponse("Totem nao encontrado");
        } else {
        	TotemService.DeleteTotem(totem.id);
            ctx.status(204);
        }
    }
	
	@OpenApi(
		    path = "/totem",
		    method = HttpMethod.POST, 
		    summary = "Adiciona um novo cadastro de totem",
		    operationId = "cadastrarTotem",
		    tags = {"Totem"},
		    requestBody = @OpenApiRequestBody(content = {@OpenApiContent(from = NewTotemRequest.class)}),		
		    responses = {
		        @OpenApiResponse(status = "201", content = {@OpenApiContent(from = Totem[].class)}),
		        @OpenApiResponse(status = "400", content = {@OpenApiContent(from = ErrorResponse.class)})
		    }
	)
    public static void addTotem(Context ctx) {
		NewTotemRequest totem = ctx.bodyAsClass(NewTotemRequest.class);
		TotemService.AddTotem(totem.numero);
        ctx.status(201);
	}
}