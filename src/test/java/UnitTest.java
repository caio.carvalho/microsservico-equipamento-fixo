import Totem.*;
import Tranca.*;
import io.javalin.http.Context;
import io.javalin.http.NotFoundResponse;
import io.javalin.plugin.json.JavalinJson;
import io.javalin.plugin.openapi.OpenApiPlugin;
import kong.unirest.Unirest;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import java.net.http.HttpResponse;
import java.util.Collection;

import org.junit.Test;

import Equipamento.ErrorResponse;

public class UnitTest {
		
	
	//Teste dos metodos referentes aos ENDPOINTS de TRANCA//  
	
	//addTranca
	@Test
	public void testAddTrancaReturns201() {
		Context ctx = mock(Context.class);
		NewTrancaRequest trancaCriada = new NewTrancaRequest(10);
		when(ctx.bodyAsClass(NewTrancaRequest.class)).thenReturn(trancaCriada);
		TrancaController.addTranca(ctx);
		verify(ctx).status(201);
	}		
	
	//getAllTrancas  
	  @Test
	  public void testGetAllTrancas() {
		  Context ctx = mock(Context.class);
		  TrancaController.getAllTrancas(ctx);
		  verify(ctx).status(200);
	  }	
	  
	//getTrancaByIdNumero	  
	  @Test
	  public void testGetTrancaByIdValidoNumeroNull() {
		  Context ctx = mock(Context.class);
		  
		  when(ctx.queryParam("trancaId")).thenReturn("5");
		  when(ctx.queryParam("numero")).thenReturn(null);
		  
		  TrancaController.getTrancaByIdNumero(ctx);
		  verify(ctx).status(200);
	  }
	  
	  @Test
	  public void testGetTrancaByIdNullNumeroNullNotFound() {
		  Context ctx = mock(Context.class);
		  
		  when(ctx.queryParam("trancaId")).thenReturn(null);
		  when(ctx.queryParam("numero")).thenReturn(null);
		  
		  NotFoundResponse thrown = assertThrows(NotFoundResponse.class,
				  () -> TrancaController.getTrancaByIdNumero(ctx));
		  assertTrue(thrown.getMessage().contains("Tranca nao encontrada"));
	  }
	  
	  @Test
	  public void testGetTrancaByIdNullNumeroValido() {
		  Context ctx = mock(Context.class);
		  
		  when(ctx.queryParam("trancaId")).thenReturn(null);
		  when(ctx.queryParam("numero")).thenReturn("123");
		  
		  TrancaController.getTrancaByIdNumero(ctx);
		  verify(ctx).status(200);
	  }
	  
	  //deleteTranca
	  @Test
	  public void testDeleteTrancaNotFound() {
		  Context ctx = mock(Context.class);
		  
		  when(ctx.queryParam("trancaId")).thenReturn("95");
		  
		  NotFoundResponse thrown = assertThrows(NotFoundResponse.class,
				  () -> TrancaController.deleteTranca(ctx));
		  assertTrue(thrown.getMessage().contains("Tranca nao encontrada"));
	  }
	  
	  @Test
	  public void testDeleteTrancaWithValidId() {
		  Context ctx = mock(Context.class);
		  
		  when(ctx.queryParam("trancaId")).thenReturn("6");
		  TrancaController.deleteTranca(ctx);
		  verify(ctx).status(204);
	  }
	  
	  //updateStatusTranca
	  @Test
	  public void testUpdateStatusTrancaToLivre() {
		  Context ctx = mock(Context.class);
		  
		  when(ctx.queryParam("trancaId")).thenReturn("1");
		  when(ctx.queryParam("statusTranca")).thenReturn("Disponivel");
		  
		  TrancaController.updateStatusTranca(ctx);
		  verify(ctx).status(204);
	  }
	  
	  @Test
	  public void testUpdateStatusTrancaToOcupada() {
		  Context ctx = mock(Context.class);
		  
		  when(ctx.queryParam("trancaId")).thenReturn("5");
		  when(ctx.queryParam("statusTranca")).thenReturn("Ocupada");
		  
		  TrancaController.updateStatusTranca(ctx);
		  verify(ctx).status(204);
	  }
	  
	  @Test
	  public void TestUpdateStatusTrancaNotFound() {
		  Context ctx = mock(Context.class);
		  
		  when(ctx.queryParam("trancaId")).thenReturn("95");
		  
		  NotFoundResponse thrown = assertThrows(NotFoundResponse.class,
				  () -> TrancaController.updateStatusTranca(ctx));
		  assertTrue(thrown.getMessage().contains("Tranca nao encontrada"));
	  }
	  
	  //getStatusTrancaById
	 
	  @Test
	  public void testGetStatusTrancaByIdValidoNumeroNull() {
		  Context ctx = mock(Context.class);
		  
		  when(ctx.queryParam("trancaId")).thenReturn("5");
		  when(ctx.queryParam("numero")).thenReturn(null);
		  
		  TrancaController.getStatusTrancaByIdNumero(ctx);
		  verify(ctx).status(200);
	  }
	  
	  @Test
	  public void testGetStatusTrancaByIdNullNumeroNullNotFound() {
		  Context ctx = mock(Context.class);
		  
		  when(ctx.queryParam("trancaId")).thenReturn(null);
		  when(ctx.queryParam("numero")).thenReturn(null);
		  
		  NotFoundResponse thrown = assertThrows(NotFoundResponse.class,
				  () -> TrancaController.getStatusTrancaByIdNumero(ctx));
		  assertTrue(thrown.getMessage().contains("Tranca nao encontrada"));
	  }
	  
	  @Test
	  public void testGetStatusTrancaByIdNullNumeroValido() {
		  Context ctx = mock(Context.class);
		  
		  when(ctx.queryParam("trancaId")).thenReturn(null);
		  when(ctx.queryParam("numero")).thenReturn("123");
		  
		  TrancaController.getStatusTrancaByIdNumero(ctx);
		  verify(ctx).status(200);
	  }
	  
	  
	//Teste dos metodos referentes aos ENDPOINTS de TOTEM//
	  
	//addTotem
		@Test
		public void testAddTotemReturns201() {
			Context ctx = mock(Context.class);
			NewTotemRequest totemCriado = new NewTotemRequest(10);
			when(ctx.bodyAsClass(NewTotemRequest.class)).thenReturn(totemCriado);
			TotemController.addTotem(ctx);
			verify(ctx).status(201);
		}		  
	  
	//getAllTotens
	  @Test
	  public void TestGetAllTotensEndpoint() {
		  Context ctx = mock(Context.class);
		  TotemController.getAllTotens(ctx);
		  verify(ctx).status(200);
	  }
	  
	//getTotemByIdNumero	  
	  @Test
	  public void TestGetTotemByIdValidoNumeroNull() {
		  Context ctx = mock(Context.class);
		  
		  when(ctx.queryParam("totemId")).thenReturn("5");
		  when(ctx.queryParam("numero")).thenReturn(null);
		  
		  TotemController.getTotemByIdNumero(ctx);
		  verify(ctx).status(200);
	  }
	  
	  @Test
	  public void TestGetTotemByIdNullNumeroNullNotFound() {
		  Context ctx = mock(Context.class);
		  
		  when(ctx.queryParam("totemId")).thenReturn(null);
		  when(ctx.queryParam("numero")).thenReturn(null);
		  
		  NotFoundResponse thrown = assertThrows(NotFoundResponse.class,
				  () -> TotemController.getTotemByIdNumero(ctx));
		  assertTrue(thrown.getMessage().contains("Totem nao encontrado"));
	  }	  	  
	  
	  @Test
	  public void TestGetTotemByIdNullNumeroValido() {
		  Context ctx = mock(Context.class);
		  
		  when(ctx.queryParam("totemId")).thenReturn(null);
		  when(ctx.queryParam("numero")).thenReturn("235");
		  
		  TotemController.getTotemByIdNumero(ctx);
		  verify(ctx).status(200);
	  }
	  
	//deleteTotem
	  @Test
	  public void TestDeleteTotemNotFound() {
		  Context ctx = mock(Context.class);
		  
		  when(ctx.queryParam("totemId")).thenReturn("15");
		  
		  NotFoundResponse thrown = assertThrows(NotFoundResponse.class,
				  () -> TotemController.deleteTotem(ctx));
		  assertTrue(thrown.getMessage().contains("Totem nao encontrado"));
	  }
	  
	  @Test
	  public void TestDeleteTotemWithValidId() {
		  Context ctx = mock(Context.class);
		  
		  when(ctx.queryParam("totemId")).thenReturn("6");
		  TotemController.deleteTotem(ctx);
		  verify(ctx).status(204);
	  }
	  
	  
	//Teste dos metodos referentes a CRUD de TOTEM 
	  
  //AddTotem
    @Test
    public void TestAddTotem() {
    	int quantidadeTotens = TotemService.totens.size();
    	int numeroTotemNovo = 2345678 ;
    	TotemService.AddTotem(numeroTotemNovo);
        assertEquals(TotemService.totens.size(), quantidadeTotens + 1);
    }
    
  //FindTotemById
    @Test
    public void TestFindTotemById() {
    	int idEsperado = 1;
    	Totem totem = TotemService.FindTotemById(idEsperado);
        int idResultado = totem.id;
        assertEquals(idEsperado, idResultado);
    }
    
  //FindTotemByNumero
    @Test
    public void TestFindTotemByNumeroValido() {
    	int numeroEnviado = 235; //numero valido
    	Totem totem = TotemService.FindTotemByNumero(numeroEnviado);
        int numeroResultado = totem.numero;
        assertEquals(numeroEnviado, numeroResultado);
    }
    
    @Test
    public void TestFindTotemByNumeroInvalido() {
    	int numeroEnviado = 876;
    	Totem totem = TotemService.FindTotemByNumero(numeroEnviado);
        assertNull(totem);
    }
    
  //GetAllTotens
    @Test
    public void TestGetAllTotens() {
    	Collection<Totem> totensEsperados = TotemService.totens.values();
    	Collection<Totem> totensResultado = TotemService.GetAllTotens();
        assertEquals(totensEsperados, totensResultado);
    }
    
  //DeleteTotem
    @Test
    public void TestDeleteTotem() {
    	int quantidadeTotens = TotemService.totens.size();
    	int idTotemDeletado = 3 ;
    	TotemService.DeleteTotem(idTotemDeletado);
        assertEquals(TotemService.totens.size(), quantidadeTotens -1);
        assertNull(TotemService.FindTotemById(idTotemDeletado));
    }    
    
//Teste dos metodos referentes a CRUD de TRANCA  
  
  //AddTranca
    @Test
    public void TestAddTranca() {
    	int quantidadeTrancasAnterior = TrancaService.trancas.size();
    	int numeroTrancaNova = 23564;
    	int numeroTrancaNova2 = 65484;
    	int numeroTrancaNova3 = 32198;
    	TrancaService.AddTranca(numeroTrancaNova);
    	TrancaService.AddTranca(numeroTrancaNova2);
    	TrancaService.AddTranca(numeroTrancaNova3);
    	int quantidadeTrancasAtual = TrancaService.trancas.size();
        assertTrue(quantidadeTrancasAtual > quantidadeTrancasAnterior);
    }
    
  //FindTrancaById
    @Test
    public void TestFindTrancaById() {
    	int idEsperado = 3;
    	Tranca tranca = TrancaService.FindTrancaById(idEsperado);
        int idResultado = tranca.id;
        assertEquals(idEsperado, idResultado);
    }
    
  //FindTrancaByNumero
    @Test
    public void TestFindTrancaByNumeroValido() {
    	int numeroEnviado = 422; //numero valido
    	Tranca tranca = TrancaService.FindTrancaByNumero(numeroEnviado);
        int numeroResultado = tranca.numero;
        assertEquals(numeroEnviado, numeroResultado);
    }
    
    @Test
    public void TestFindTrancaByNumeroInvalido() {
    	int numeroEnviado = 0;
    	Tranca tranca = TrancaService.FindTrancaByNumero(numeroEnviado);
        assertNull(tranca);
    }  
        
  //GetAllTrancas
    @Test
    public void TestGetAllTrancas() {
    	Collection<Tranca> trancasEsperadas = TrancaService.trancas.values();
    	Collection<Tranca> trancasResultado = TrancaService.GetAllTrancas();
        assertEquals(trancasEsperadas, trancasResultado);
    }
    
  //DeleteTranca
    @Test
    public void TestDeleteTranca() {
    	int quantidadeTrancas = TrancaService.trancas.size();
    	int idTrancaDeletada = 2 ;
    	TrancaService.DeleteTranca(idTrancaDeletada);
        assertEquals(TrancaService.trancas.size(), quantidadeTrancas -1);
        assertNull(TrancaService.FindTrancaById(idTrancaDeletada));
    } 
    
  //UpdateStatusTranca
    @Test
    public void TestUpdateStatusTranca() {
    	TrancaService.trancas.put(5, new Tranca(5, 456, "Em uso"));
    	Tranca tranca = TrancaService.FindTrancaById(5);
    	TrancaService.UpdateStatusTranca(tranca.id, tranca.numero, "Disponivel");
    	Tranca trancaAtualizada = TrancaService.FindTrancaById(5);
    	assertEquals(trancaAtualizada.status, "Disponivel");
    } 
    
  //GetStatusTrancaById
    @Test
    public void testGetStatusTrancaById() {
    	String resultadoEsperado1 = "Em Reparo";
    	String resultadoEsperado2 = "Disponivel";
    	String statusTranca = TrancaService.getStatusTrancaById(8);
    	assertTrue(statusTranca.equals(resultadoEsperado1));    
    	statusTranca = TrancaService.getStatusTrancaById(7);    	
    	assertTrue(statusTranca.equals(resultadoEsperado2));
    } 
    
   //GetStatusTrancaByNumero
    @Test
    public void testGetStatusTrancaByNumero() {
    	String resultadoEsperado1 = "Em Reparo";
    	String resultadoEsperado2 = "Disponivel";
    	String statusTranca = TrancaService.GetStatusTrancaByNumero(444);
    	assertTrue(statusTranca.equals(resultadoEsperado1));    
    	statusTranca = TrancaService.GetStatusTrancaByNumero(666);    	
    	assertTrue(statusTranca.equals(resultadoEsperado2));
    } 
    
    
//Teste dos Construtores
    
    //Totem
    @Test
    public void TestConstrutorTotem() {
    	Totem totemTeste = new Totem(6, 10); 
    	int totemIdEsperado = 6;
    	int totemNumeroEsperado = 10;
    	assertEquals(totemIdEsperado, totemTeste.id);
    	assertEquals(totemNumeroEsperado, totemTeste.numero);
    }
    
    //NewTotemRequest
    @Test
    public void TestConstrutorNewTotemRequestVazio() {
    	NewTotemRequest totemRequestTeste = new NewTotemRequest();
    	assertNotNull(totemRequestTeste);
    }
    
    @Test
    public void TestConstrutorNewTotemRequest() {
    	NewTotemRequest totemRequestTeste = new NewTotemRequest(2546);
    	int totemNumeroEsperado = 2546;
    	assertNotNull(totemRequestTeste);
    	assertEquals(totemNumeroEsperado, totemRequestTeste.numero);
    }
    
    @Test
    public void testConstrutorTotemService() {
    	TotemService totemServiceTeste = new TotemService();
    	assertNotNull(totemServiceTeste);
    }
    
    @Test
    public void testConstrutorTotemController() {
    	TotemController totemControllerTeste = new TotemController();
    	assertNotNull(totemControllerTeste);
    }
    
    //Tranca
    @Test
    public void TestConstrutorTranca() {
    	Tranca trancaTeste = new Tranca(9, 876, "Em uso"); 
    	int trancaIdEsperado = 9;
    	int trancaNumeroEsperado = 876;
    	String statusTrancaEsperado ="Em uso";
    	assertEquals(trancaIdEsperado, trancaTeste.id);
    	assertEquals(trancaNumeroEsperado, trancaTeste.numero);
    	assertEquals(statusTrancaEsperado, trancaTeste.status);
    }
    
    @Test
    public void TestConstrutorNewTrancaRequestVazio() {
    	NewTrancaRequest trancaRequestTeste = new NewTrancaRequest();
    	assertNotNull(trancaRequestTeste);
    }
    
    @Test
    public void TestConstrutorNewTrancaRequest() {
    	NewTrancaRequest trancaRequestTeste = new NewTrancaRequest(98456);
    	int trancaNumeroEsperado = 98456;
    	assertNotNull(trancaRequestTeste);
    	assertEquals(trancaNumeroEsperado, trancaRequestTeste.numero);
    }
    
    @Test
    public void testConstrutorTrancaService() {
    	TrancaService trancaServiceTeste = new TrancaService();
    	assertNotNull(trancaServiceTeste);
    }
    
    @Test
    public void testConstrutorTrancaController() {
    	TrancaController trancaControllerTeste = new TrancaController();
    	assertNotNull(trancaControllerTeste);
    }
    
  //ErrorResponse
   @Test
   public void testErrorResponse() {
	   ErrorResponse errorResponseTeste = new ErrorResponse();
	   assertNotNull(errorResponseTeste);
   }
   
}
