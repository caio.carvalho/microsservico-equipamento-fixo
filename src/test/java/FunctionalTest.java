import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import Equipamento.*;
import kong.unirest.Unirest;



public class FunctionalTest {
	
	 private JavalinApp app = new JavalinApp();	 

	   @Test
	   public void GET_to_fetch_trancas_returns_list_of_trancas() {
		   app.start(1234);
	       kong.unirest.HttpResponse<String> response = Unirest.get("http://localhost:1234/tranca").asString();
	       assertEquals(response.getStatus(),200);
	       assertTrue(response.getBody().contains("\"id\":9,\"numero\":9874,\"status\":\"Ocupada\""));
	       assertTrue(response.getBody().contains("\"id\":10,\"numero\":985,\"status\":\"Disponivel\""));
	       app.stop();
	   }

	   @Test
	   public void GET_to_fetch_tranca_by_id_returns_tranca9() {
		   app.start(1234);
	       kong.unirest.HttpResponse<String> response = Unirest.get("http://localhost:1234/tranca/buscaPorId?trancaId=9").asString();
	       assertEquals(response.getStatus(),200);
	       assertEquals(response.getBody(),("{\"id\":9,\"numero\":9874,\"status\":\"Ocupada\"}"));
	       app.stop();
	   }
	   
	   @Test
	   public void GET_to_fetch_tranca_by_numero_returns_tranca10() {
		   app.start(1234);
	       kong.unirest.HttpResponse<String> response = Unirest.get("http://localhost:1234/tranca/buscaPorId?numero=985").asString();
	       assertEquals(response.getStatus(),200);
	       assertEquals(response.getBody(),("{\"id\":10,\"numero\":985,\"status\":\"Disponivel\"}"));
	       app.stop();
	   }   
	   
	   @Test
	   public void GET_to_fetch_tranca_by_numero_returns_404() {
		   app.start(1234);
	       kong.unirest.HttpResponse<String> response = Unirest.get("http://localhost:1234/tranca/buscaPorId?numero=7778").asString();
	       assertEquals(response.getStatus(),404);
	       app.stop();
	   }   	   
	   
	   @Test
	   public void GET_to_fetch_status_tranca_by_numero_returns_status_tranca9() {
		   app.start(1234);
	       kong.unirest.HttpResponse<String> response = Unirest.get("http://localhost:1234/statusTranca?numero=9874").asString();
	       assertEquals(response.getStatus(),200);
	       assertEquals(response.getBody(), "\"Ocupada\"");
	       app.stop();
	   }
 
	   @Test
	   public void GET_to_fetch_status_tranca_by_id_returns_status_tranca10() {
		   app.start(1234);
	       kong.unirest.HttpResponse<String> response = Unirest.get("http://localhost:1234/statusTranca?trancaId=10").asString();
	       assertEquals(response.getStatus(),200);
	       assertEquals(response.getBody(), "\"Disponivel\"");
	       app.stop();
	   }
	   
	   @Test
	   public void GET_to_fetch_status_tranca_by_id_returns_404() {
		   app.start(1234);
	       kong.unirest.HttpResponse<String> response = Unirest.get("http://localhost:1234/statusTranca?id=15484").asString();
	       assertEquals(response.getStatus(),404);
	       app.stop();
	   }
	   
	   @Test
	   public void POST_tranca_returns_201() {
		   app.start(1236);
	       kong.unirest.HttpResponse<String> response = Unirest.post("http://localhost:1236/tranca").body("{\r\n\"numero\": 23478\r\n}").asString();
	       assertEquals(response.getStatus(),201);
	       app.stop();
	   }
	   
	   @Test
	   public void DELETE_tranca_returns_204() {
		   app.start(1235);
	       kong.unirest.HttpResponse<String> response = Unirest.delete("http://localhost:1235/tranca/deletaPorId?trancaId=11").asString();
	       assertEquals(response.getStatus(),204);
	       kong.unirest.HttpResponse<String> response2 = Unirest.get("http://localhost:1235/tranca/buscaPorId?trancaId=11").asString();
	       assertEquals(response2.getStatus(),404);
	       app.stop();
	   }
	   
	   @Test
	   public void PATCH_tranca_returns_204() {
		   app.start(8888);
	       kong.unirest.HttpResponse<String> response = Unirest.patch("http://localhost:8888/statusTranca?trancaId=12&statusTranca=Em Reparo").asString();
	       assertEquals(response.getStatus(),204);
	       kong.unirest.HttpResponse<String> response2 = Unirest.get("http://localhost:8888/statusTranca?trancaId=12").asString();
	       assertEquals(response2.getStatus(),200);
	       assertEquals(response2.getBody(), "\"Em Reparo\"");
	       app.stop();
	   }
	   
	   @Test
	   public void PATCH_libera_tranca_returns_400_reparo() {
		   app.start(9854);
	       kong.unirest.HttpResponse<String> response = Unirest.patch("http://localhost:9854/liberaTranca?trancaId=15").asString();
	       assertEquals(response.getStatus(),400);
	       app.stop();
	   }
	   
	   @Test
	   public void PATCH_libera_tranca_returns_400_aposentada() {
		   app.start(9855);
	       kong.unirest.HttpResponse<String> response = Unirest.patch("http://localhost:9855/liberaTranca?trancaId=13").asString();
	       assertEquals(response.getStatus(),400);
	       app.stop();
	   }
	   
	   @Test
	   public void PATCH_libera_tranca_returns_400_disponivel() {
		   app.start(9856);
	       kong.unirest.HttpResponse<String> response = Unirest.patch("http://localhost:9856/liberaTranca?trancaId=12").asString();
	       assertEquals(response.getStatus(),400);
	       app.stop();
	   }
	   
	   @Test
	   public void PATCH_libera_tranca_returns_404() {
		   app.start(8451);
	       kong.unirest.HttpResponse<String> response = Unirest.patch("http://localhost:8451/liberaTranca?trancaId=84").asString();
	       assertEquals(response.getStatus(),404);
	       app.stop();
	   }	
	   
	   @Test
	   public void PATCH_libera_tranca_returns_200() {
		   app.start(9845);
	       kong.unirest.HttpResponse<String> response = Unirest.patch("http://localhost:9845/liberaTranca?trancaId=14").asString();
	       assertEquals(response.getStatus(),204);
	       app.stop();
	   }	   
	   
	   @Test
	   public void GET_to_fetch_totens_returns_list_of_totens() {
		   app.start(1234);
	       kong.unirest.HttpResponse<String> response = Unirest.get("http://localhost:1234/totem").asString();
	       assertEquals(response.getStatus(),200);
	       assertTrue(response.getBody().contains("{\"id\":9,\"numero\":652}"));
	       assertTrue(response.getBody().contains("{\"id\":8,\"numero\":347}"));
	       app.stop();
	   }
	   
	   @Test
	   public void POST_totem_returns_201() {
		   app.start(1238);
	       kong.unirest.HttpResponse<String> response = Unirest.post("http://localhost:1238/totem").body("{\r\n\"numero\": 5674\r\n}").asString();
	       assertEquals(response.getStatus(),201);
	       app.stop();
	   }
	
	   @Test
	   public void GET_to_fetch_totem_by_id_returns_totem8() {
		   app.start(1239);
	       kong.unirest.HttpResponse<String> response = Unirest.get("http://localhost:1239/totem/buscaPorId?totemId=8").asString();
	       assertEquals(response.getStatus(),200);
	       assertEquals(response.getBody(),("{\"id\":8,\"numero\":347}"));
	       app.stop();
	   }
	   
	   @Test
	   public void GET_to_fetch_totem_by_numero_returns_totem9() {
		   app.start(1240);
	       kong.unirest.HttpResponse<String> response = Unirest.get("http://localhost:1240/totem/buscaPorId?numero=652").asString();
	       assertEquals(response.getStatus(),200);
	       assertEquals(response.getBody(),("{\"id\":9,\"numero\":652}"));
	       app.stop();
	   }  
	   
	   @Test
	   public void GET_to_fetch_totem_by_numero_returns_404() {
		   app.start(1240);
	       kong.unirest.HttpResponse<String> response = Unirest.get("http://localhost:1240/totem/buscaPorId?numero=99999").asString();
	       assertEquals(response.getStatus(),404);
	       app.stop();
	   }  
	   
	   @Test
	   public void DELETE_totem_returns_204() {
		   app.start(1241);
	       kong.unirest.HttpResponse<String> response = Unirest.delete("http://localhost:1241/totem/deletaPorId?totemId=7").asString();
	       assertEquals(response.getStatus(),204);
	       kong.unirest.HttpResponse<String> response2 = Unirest.get("http://localhost:1241/totem/buscaPorId?totemId=7").asString();
	       assertEquals(response2.getStatus(),404);
	       app.stop();
	   }
}